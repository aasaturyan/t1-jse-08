package ru.t1.aasaturyan.tm;

import ru.t1.aasaturyan.tm.api.IArgumentRepository;
import ru.t1.aasaturyan.tm.api.ICommandRepository;
import ru.t1.aasaturyan.tm.constant.ArgumentConst;
import ru.t1.aasaturyan.tm.constant.CommandConst;
import ru.t1.aasaturyan.tm.model.Command;
import ru.t1.aasaturyan.tm.model.Argument;
import ru.t1.aasaturyan.tm.repository.ArgumentRepository;
import ru.t1.aasaturyan.tm.repository.CommandRepository;
import ru.t1.aasaturyan.tm.util.FormatUtil;

import java.util.Scanner;

public final class Application {

    private static final ICommandRepository COMMAND_REPOSITORY = new CommandRepository();

    private static final IArgumentRepository ARGUMENT_REPOSITORY = new ArgumentRepository();

    public static void main(String[] args) {
        processArguments(args);
        processCommands();
    }

    public static void processArguments(String[] args) {
        if (args == null || args.length == 0) return;
        final String argument = args[0];
        processArgument(argument);
    }

    public static void processCommands() {
        final Scanner scanner = new Scanner(System.in);
        System.out.println("** WELCOME TO TASK MANAGER **");
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND: ");
            final String command = scanner.nextLine();
            processCommand(command);
        }
    }

    private static void processCommand(String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.HELP:
                showHelpCommand();
                break;
            case CommandConst.VERSION:
                showVersion();
                break;
            case CommandConst.ABOUT:
                showAbout();
                break;
            case CommandConst.EXIT:
                showExit();
                break;
            case CommandConst.INFO:
                showSystemInfo();
                break;
            default:
                showErrorCommand();
                break;
        }
    }

    private static void processArgument(String argument) {
        if (argument == null || argument.isEmpty()) return;
        switch (argument) {
            case ArgumentConst.HELP:
                showHelpArg();
                break;
            case ArgumentConst.VERSION:
                showVersion();
                break;
            case ArgumentConst.ABOUT:
                showAbout();
                break;
            case ArgumentConst.INFO:
                showSystemInfo();
                break;
            default:
                showErrorArg();
                break;
        }
        System.exit(0);
    }

    private static void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final long availableProcessors = runtime.availableProcessors();
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = FormatUtil.format(freeMemory);
        final long maxMemory = runtime.maxMemory();
        final boolean maxMemoryCheck = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = maxMemoryCheck ? "no limit" : FormatUtil.format(maxMemory);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = FormatUtil.format(totalMemory);
        final long usageMemory = totalMemory - freeMemory;
        final String usageMemoryFormat = FormatUtil.format(usageMemory);

        System.out.println("Available processors (cores): " + availableProcessors);
        System.out.println("Usage memory: " + usageMemoryFormat);
        System.out.println("Maximum memory: " + maxMemoryFormat);
        System.out.println("Free memory: " + freeMemoryFormat);
        System.out.println("Total memory: " + totalMemoryFormat);
    }

    private static void showExit() {
        System.out.println("[EXIT]");
        System.exit(0);
    }

    private static void showErrorArg() {
        System.err.println("[ERROR]");
        System.err.println("This argument is not supported...");
        System.exit(1);
    }

    private static void showErrorCommand() {
        System.err.println("[ERROR]");
        System.err.println("This command is not supported...");
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("name: Artyom Asaturyan");
        System.out.println("email: aasaturyan@tech-code.ru");
    }

    private static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.8.2");
    }

    private static void showHelpArg() {
        System.out.println("[HELP]");
        for (final Argument argument : ARGUMENT_REPOSITORY.getArguments())
            System.out.println(argument);
    }

    private static void showHelpCommand() {
        System.out.println("[HELP]");
        for (final Command command : COMMAND_REPOSITORY.getCommands())
            System.out.println(command);
    }

}
