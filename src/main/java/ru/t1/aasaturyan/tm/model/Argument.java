package ru.t1.aasaturyan.tm.model;

public final class Argument {

    private String name;

    private String description;

    public Argument() {
    }

    public Argument(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        String result = "";
        if (name != null && !name.isEmpty())
            result += name;
        if (description != null && !description.isEmpty())
            result += ": " + description;
        return result;
    }
}
