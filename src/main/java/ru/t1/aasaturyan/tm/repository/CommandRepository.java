package ru.t1.aasaturyan.tm.repository;

import ru.t1.aasaturyan.tm.api.ICommandRepository;
import ru.t1.aasaturyan.tm.constant.CommandConst;
import ru.t1.aasaturyan.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command INFO = new Command(
            CommandConst.INFO,
            "Show system info."
    );

    private static final Command ABOUT = new Command(
            CommandConst.ABOUT,
            "Show developer info."
    );

    private static final Command HELP = new Command(
            CommandConst.HELP,
            "Show command list"
    );

    private static final Command VERSION = new Command(
            CommandConst.VERSION,
            "Show application version."
    );

    private static final Command EXIT = new Command(
            CommandConst.EXIT,
            "Close application."
    );

    public static final Command[] COMMANDS = new Command[]{
            ABOUT, INFO, VERSION, HELP, EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
